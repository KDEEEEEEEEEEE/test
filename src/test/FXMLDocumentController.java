/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package test;

import java.net.URL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 *
 * @author CSIE
 */
public class FXMLDocumentController implements Initializable {
    
    @FXML
    private Label label;
    
    @FXML
    private void handleButtonAction(ActionEvent event) {
        try {
            Connection conn = DriverManager.getConnection("jdbc:derby://localhost:1527/test","test","test") ;
            if(conn != null)
            {
                label.setText("linked");
            }
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
            label.setText("failed");    
        }
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    private int doInsert() {
         int rows=0;
        String insert="insert into TEST.COLLEAGUES values(?,?,?,?,?,?)";
        
        PreparedStatement stmt =conn. prepareStatement(insert);
        try {
            stmt.setInt(1,2);
            stmt.setString(2,"肉呆");
            stmt.setString(3,"五告");
            stmt.setString(4,"天兵二號");
            stmt.setString(5,"天兵組");
            stmt.setString(6,"test@test.com.tw");
            rows = stmt.executeUpdate();
            stmt.close();
        } catch (SQLException ex) {
            Logger.getLogger(FXMLDocumentController.class.getName()).log(Level.SEVERE, null, ex);
        }finally{
        return rows ;
        }
    }
}
